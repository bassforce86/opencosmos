package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/bassforce86/opencosmos/models"
)

func Index(w http.ResponseWriter, r *http.Request) {
	renderJSON(w, http.StatusOK, struct {
		Message string `json:"message"`
	}{
		Message: "Welcome to the OpenCosmos Telemetry API",
	})
}

func SatelliteID(w http.ResponseWriter, r *http.Request) {
	id := models.ParseTelemetryID(mux.Vars(r)["id"])
	telemetry := models.NewTelemetry(id)
	queryAPI := dbClient.QueryAPI(influx.Organisation)
	payload := struct {
		Total     int              `json:"total"`
		Telemetry models.Telemetry `json:"telemetry"`
		Errors    []string         `json:"errors"`
	}{
		Telemetry: *telemetry,
	}

	from := timePeriod(r.FormValue("from"), 5)
	to := timePeriod(r.FormValue("to"), 0)

	if isValidTimeTo(from, to) {
		var builder strings.Builder
		fmt.Fprintf(&builder, "from(bucket: \"%s\")", influx.Bucket)
		if from := r.FormValue("from"); from != "" {
			if to := r.FormValue("to"); to != "" {
				fmt.Fprintf(&builder, "|> range(start: %s, stop: %s)", from, to)
			} else {
				fmt.Fprintf(&builder, "|> range(start: %s)", from)
			}
		} else {
			fmt.Fprintf(&builder, "|> range(start: -5m)")
		}

		fmt.Fprintf(&builder, "|> filter(fn: (r) => r.satellite == \"%s\")", mux.Vars(r)["id"])
		fmt.Fprint(&builder, `|> sort(columns:["_time"]) |> group(columns: ["_time"])`)

		query := builder.String()
		result, err := queryAPI.Query(context.Background(), query)
		if err == nil {
			// Iterate over query response
			for result.Next() {
				payload.Telemetry.AddPoint(&models.Point{
					TelemetryID: id,
					Timestamp:   result.Record().Time().Unix(),
					Value:       float32(result.Record().Value().(float64)),
				})
			}
			// check for an error
			if result.Err() != nil {
				payload.Errors = append(payload.Errors, fmt.Sprintf("query parsing error: %s\n", result.Err().Error()))
				fmt.Println(fmt.Sprintf("query parsing error: %s\n", result.Err().Error()))
			}
		} else {
			payload.Errors = append(payload.Errors, fmt.Sprintf("error: %s\n", err.Error()))
			fmt.Println(fmt.Sprintf("error: %s\n", err.Error()))
		}
	} else {
		payload.Errors = append(payload.Errors, "invalid time period selected")
	}

	payload.Total = len(payload.Telemetry.Points)

	renderJSON(w, http.StatusOK, payload)
}

func renderJSON(w http.ResponseWriter, status int, payload interface{}) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(payload)
}

func timePeriod(time string, def int) int {
	switch time {
	case "-1m":
		return 1
	case "-5m":
		return 5
	case "-10m":
		return 10
	case "-15m":
		return 15
	case "-30m":
		return 30
	case "-1h":
		return 60
	default:
		return def
	}
}

func isValidTimeTo(f, t int) bool {
	return f > t
}
