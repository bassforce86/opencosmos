module gitlab.com/bassforce86/opencosmos/api

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/influxdata/influxdb-client-go/v2 v2.4.0
	gitlab.com/bassforce86/opencosmos/connections v0.0.14
	gitlab.com/bassforce86/opencosmos/models v0.0.29
)
