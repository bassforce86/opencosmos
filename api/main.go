package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"gitlab.com/bassforce86/opencosmos/connections"
	"gitlab.com/bassforce86/opencosmos/models"
)

var (
	router   = mux.NewRouter().StrictSlash(true)
	influx   *connections.InfluxDBConnection
	dbClient influxdb2.Client
)

func main() {
	influx = connections.InfluxDB()
	dbClient = models.InitializeDB(influx)
	defer dbClient.Close()

	router.Path("/").HandlerFunc(Index).Methods("GET", "OPTIONS")
	router.Path("/satellites/{id:[0-9]+}").
		HandlerFunc(SatelliteID).Methods("GET", "OPTIONS")
	router.Path("/satellites/{id:[0-9]+}").
		Queries(
			"from", "{from}",
			"to", "{to}",
		).
		HandlerFunc(SatelliteID).Methods("GET", "OPTIONS")

	fmt.Println("Listening on: :3000")
	log.Fatal(http.ListenAndServe(":3000", router))
}
