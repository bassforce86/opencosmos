module gitlab.com/bassforce86/opencosmos/listener

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	gitlab.com/bassforce86/opencosmos/connections v0.0.14
	gitlab.com/bassforce86/opencosmos/models v0.0.29
)
