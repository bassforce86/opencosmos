package main

import (
	"fmt"

	"gitlab.com/bassforce86/opencosmos/connections"
	"gitlab.com/bassforce86/opencosmos/models"
)

var influx *connections.InfluxDBConnection

func main() {
	var err error

	influx = connections.InfluxDB()
	dbClient := models.InitializeDB(influx)
	defer dbClient.Close()

	telemetry := connections.Telemetry()
	telemetryClient := telemetry.Connect()
	defer telemetryClient.Close()
	reader := models.GetReaderType()
	fmt.Printf("reader type: %s\n", reader.Type)

	writeAPI := dbClient.WriteAPI(influx.Organisation, influx.Bucket)
	count := 0
	// listen for reply
	for {
		p := models.NewPoint()

		if reader.Type == models.STRING {
			p, err = p.DecodeFromString(telemetryClient)
		}
		if reader.Type == models.BINARY {
			p, err = p.DecodeFromBinary(telemetryClient)
		}

		if err != nil {
			fmt.Printf("error: %s\n", err.Error())
		} else {
			count = count + 1
			p.Save(writeAPI)
		}
		if count > int(influx.BatchSize) {
			count = 0
			writeAPI.Flush()
			fmt.Println("flushing influx writes...")
		}
	}
}
