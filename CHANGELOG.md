# CHANGELOG

<!--- next entry here -->

## 0.2.2
2021-08-10

### Fixes

- minor tweaks to docker files (c077e6bd1d644f8ee1f0d8a749f8711029fcb471)

## 0.2.1
2021-08-06

### Fixes

- models refinement (727b0d6f5af28a30713847861d738df0cf8df101)
- binary decoding fix (85c498b24f6c06b42232068e149b300a06f2d8cc)
- point binary unmarshal fix (3a7f6781e6b5e5dc7aab5340ed72b0c1c25679c9)
- reverted back to default struct (9b1ae86491b0665b117c85e0de39cb04a49652ad)
- update serialization order (0478c3f38220b9caa92b6fc8ad873a25294d79f2)
- add random nano sec to define separate points (d6a3841633f7097d27148e168434535f14761785)
- re-introduce uuid tag (cc3d43a3a9a848b639d928614edfab4fab3236ed)
- minor error hanlding update (ae2f6769c1a06978757bde615678a47a10ed3ee9)

## 0.2.0
2021-08-04

### Features

- updated api to influx call (06fe07301d22ef17f2b504f911e1d5ae4f1cd719)
- added basic frontend (8a77f6b697665a58c231cbf195804b65c4607402)

## 0.1.0
2021-08-03

### Features

- added influx db connection (687236e15e465d94c145c1721eb9fd915ba99ae0)
- updated Models with influx ref (4cc8370982896506365341fda2827ca5996ae350)
- adding influx to save point (90659dec936e64fcb82891ee679af9ec8ff9923e)
- new connections release (88d9c124689eb70f574891f0c83149c4cf3ad796)
- models release (c264986850870b196f23eb03652f5c974ce7710f)
- models update (88019e2217289439edc5548c271a6d0e5d4f6b4a)
- removed redis, added error catch to string reader (dd07d9ba6add54591aecc7a03095ac28e52c47ec)
- connections revamp, removed Redis (ce81e1c1cfe5ef48938d9c7512b3790edf4641c9)
- removed Redis (64ab43781b8df8deb335c2b4d4f9adb29d60cb23)
- connections upgrade refactor (47094bf545a85376e6e022e5b0de0245d074f5f4)
- models update (3b1d8455b470e94dc626d851c8aeb7b8ae517fda)
- listener update (9c397b61ac9b3ac98ed44bcaccc64e75f96772f3)
- updated naming (a8e6dfe1c5f2dabe4100795a047fced1d1b84646)

### Fixes

- made parsing func's public (fb34ba2fd0ccaea4dd61409f3a434e7f9ce6270a)
- updated Point model (7141abbedf3ed101c89bddf37fc9f537bebf82c7)
- update point save method (67f6292755dd8b5ac242a98b98c0021ebe3edaf4)
- missed token default in influx (51d32331e6e7c3235f4184ec7791c52b2874df09)
- add flush to point save (cdbd7aecfd9a86679343b8061991943a736f698d)
- bump batch to 200 (83c17057ce45fd6813a063029697c0ca3b9f7e6c)
- EOD update - still no tags / values in influx (cec9d5ef818b1d79b5f0c517562f4bbf47545e83)

## 0.0.4
2021-08-02

### Fixes

- removed save from AddPoint (aae4085cab09005fcc4765e7269df1e6f3e7d4a4)
- unexported header (345fef9b58c4bbd49be41096a0e090dad9816838)

## 0.0.3
2021-08-02

### Fixes

- update connection to reference uuid (95ea75c9b7c4197ab940117176c98a7ae1f4edc3)
- update connection package (a67be7010ed867a0cd49cafa012f67d808789c6e)
- bump to v0.0.1 (ebff66688fa0e49226bf9294bcb66dd8970addb7)
- major rebuild (9b41de7970d169d4259538e5538595bd0efa468b)
- update dependencies (c48d5080780e41a55234cb0fb3305f6675dfb71b)
- updating connections (ea481dd5b82325b38af2fd6d4a5b19b157a45dbb)
- update deps (082037e5465f14057112096d9bb0824611d3a457)
- publicized connection struct (ccb12bca08d3b2dc211b263c1fc08182b4094a81)
- update deps (236c1bedc766a62ce069db14fda1eafdda9a04d7)
- move types to root folder (2dcb3da3a2f6191d20f2500f1908525680a80fe7)
- update point find method (6835eb296ae8ab7f1c1c0fa63efb015ec1bc66a7)
- updated telemetry model (9968df015b6fd503f344c4239cee8c10051e7d36)
- dep update (1744c07f83e8c92eb00ed0a98ab9b083076e608f)
- up connection tag (c9a4869a9247d8db34d52a17995eb1d62ef99e67)
- minor tweaks (b234a7c68834d1b3fb0517bfec2b4afe41251139)
- updated models and connections (abfe1908f53979309905c1597b07a34d543a80bb)
- rename package to connections (92790350e0c68c7fad3677e4980d1a2bd6b9261c)
- updated redis client (cc312e94da5c7d216a20390dbb6d50b21a1ac27d)

## 0.0.2
2021-08-01

### Fixes

- updating dependencies (f861daa13931934d71b82ee5d17741006598389c)