module gitlab.com/bassforce86/opencosmos/models

go 1.16

require (
	github.com/influxdata/influxdb-client-go/v2 v2.4.0
	gitlab.com/bassforce86/opencosmos/connections v0.0.14
)
