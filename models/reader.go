package models

import (
	"os"
	"strings"
)

const (
	BINARY = "binary"
	STRING = "string"
)

type Reader struct {
	Type string
}

func GetReaderType() Reader {
	var reader Reader

	reader.Type = strings.ToLower(os.Getenv("READ_TYPE"))
	if reader.Type == "" {
		reader.Type = BINARY
	}

	return reader
}
