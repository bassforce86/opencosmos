package models

import (
	"fmt"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"gitlab.com/bassforce86/opencosmos/connections"
)

var (
	influxDb influxdb2.Client
	influx   *connections.InfluxDBConnection
)

func start() {
	if influx == nil {
		influx = connections.InfluxDB()
	}
	if influxDb == nil {
		influxDb = influx.Connect()
	}
}

func InitializeDB(flux *connections.InfluxDBConnection) influxdb2.Client {
	influx = flux
	influxDb = influx.Connect()
	fmt.Printf("Model DB initialized: %v\n", influx)
	return influxDb
}
