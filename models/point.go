package models

import (
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
)

// Point describes a single data point for a satallite
type Point struct {
	Header      [4]byte `json:"-"`
	Timestamp   int64   `json:"timestamp"`
	TelemetryID uint16  `json:"telemetry_id"`
	Value       float32 `json:"value"`
}

// NewPoint returns a defaulted Point struct
func NewPoint() *Point {
	p := &Point{
		TelemetryID: 1,
		Timestamp:   1,
		Value:       1,
	}
	return p
}

// Save sends a single point to the data store
func (p *Point) Save(wapi api.WriteAPI) {
	start()
	dp := influxdb2.NewPointWithMeasurement("telemetry").
		AddTag("satellite", strconv.Itoa(int(p.TelemetryID))).
		AddTag("uuid", uuid.NewString()).
		AddField("value", p.Value).
		SetTime(time.Unix(p.Timestamp, 0))
	wapi.WritePoint(dp)
	fmt.Printf("Point Saved: %+v\n", p)
}

// DecodeFromBinary decodes the binary information from a satellite into a data point
func (p *Point) DecodeFromBinary(conn net.Conn) (*Point, error) {
	if err := binary.Read(conn, binary.LittleEndian, p); err != nil {
		return p, err
	}
	return p, nil
}

// DecodeFromString decodes the string information from a satellite into a data point
func (p *Point) DecodeFromString(conn net.Conn) (*Point, error) {
	buff := make([]byte, 25)

	n, err := conn.Read(buff)
	if err != nil {
		return p, err
	}

	message := string(buff[:n])
	message = strings.TrimPrefix(message, "[")
	message = strings.TrimSuffix(message, "]")
	data := strings.Split(message, ":")

	if len(data) < 2 {
		return p, fmt.Errorf("invalid telemetry received: %s", message)
	}

	p.TelemetryID = ParseTelemetryID(data[1])
	p.Timestamp = ParseTimestamp(data[0])
	p.Value = ParseValue(data[2])

	return p, nil
}

// ParseTimestamp parses a string into an int64 (Point compatible Timestamp)
func ParseTimestamp(ts string) int64 {
	timestamp, err := strconv.ParseInt(ts, 10, 64)
	if err != nil {
		fmt.Printf("error: %s", err.Error())
		return 0
	}
	return int64(timestamp)
}

// ParseTelemetryID parses a string into an uint16 (Point compatible TelemetryID)
func ParseTelemetryID(id string) uint16 {
	telemetryId, err := strconv.ParseUint(id, 0, 16)
	if err != nil {
		fmt.Printf("error: %s", err.Error())
		return 0
	}
	return uint16(telemetryId)
}

// ParseValue parses a string into an float32 (Point compatible Value)
func ParseValue(v string) float32 {
	value, err := strconv.ParseFloat(v, 32)
	if err != nil {
		fmt.Printf("error: %s", err.Error())
		return 0
	}
	return float32(value)
}
