package models

// Telemetry describes a set of Points attributed to a single satellite
// Including the coresponding Satellite's ID
type Telemetry struct {
	ID     uint16  `json:"id"`
	Points []Point `json:"points"`
}

// NewTelemetry returns a default Telemtry struct
func NewTelemetry(id uint16) *Telemetry {
	return &Telemetry{ID: id}
}

// AddPoint adds a point to the Telemetry Points Array
func (t *Telemetry) AddPoint(point *Point) {
	t.Points = append(t.Points, *point)
}
