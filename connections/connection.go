package connections

import (
	"os"
	"strconv"
)

type Connection struct {
	Protocol string
	Host     string
	Port     string
}

func getStringEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getUIntEnv(key string, fallback uint64) uint64 {
	if value, ok := os.LookupEnv(key); ok {
		s, err := strconv.Atoi(value)
		if err != nil {
			return fallback
		}
		return uint64(s)
	}
	return fallback
}

func getIntEnv(key string, fallback int64) int64 {
	if value, ok := os.LookupEnv(key); ok {
		s, err := strconv.Atoi(value)
		if err != nil {
			return fallback
		}
		return int64(s)
	}
	return fallback
}
