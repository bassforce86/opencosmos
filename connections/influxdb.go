package connections

import (
	"fmt"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

const influxPrefix = "INFLUXDB"

type InfluxDBConnection struct {
	base         Connection
	Token        string
	Organisation string
	Bucket       string
	BatchSize    uint64
	Username     string
	Password     string
}

func InfluxDB() *InfluxDBConnection {
	return &InfluxDBConnection{
		base: Connection{
			Protocol: getInfluxProtocol("http"),
			Host:     getInfluxHost("influxdb"),
			Port:     getInfluxPort("8086"),
		},
		Token:        getInfluxToken("test_token"),
		Organisation: getInfluxOrganisation("opencosmos"),
		Bucket:       getInfluxBucket("satellites"),
		BatchSize:    getInfluxBatchSize(200),
		Username:     getInfluxUsername("oc_username"),
		Password:     getInfluxPassword("oc_password"),
	}
}

func (c *InfluxDBConnection) Connect() influxdb2.Client {
	client := influxdb2.NewClientWithOptions(
		fmt.Sprintf("%s://%s:%s", c.Protocol(), c.Host(), c.Port()),
		c.Token,
		influxdb2.DefaultOptions().SetBatchSize(uint(c.BatchSize)),
	)
	fmt.Printf("Connected to InfluxDB: %+v\n", c)
	return client
}

func (c *InfluxDBConnection) Host() string {
	return c.base.Host
}

func (c *InfluxDBConnection) Port() string {
	return c.base.Port
}

func (c *InfluxDBConnection) Protocol() string {
	return c.base.Protocol
}

func getInfluxProtocol(fallback string) string {
	return getStringEnv(influxPrefix+"_PROTOCOL", fallback)
}

func getInfluxHost(fallback string) string {
	return getStringEnv(influxPrefix+"_HOST", fallback)
}

func getInfluxPort(fallback string) string {
	return getStringEnv(influxPrefix+"_PORT", fallback)
}

func getInfluxToken(fallback string) string {
	return getStringEnv(influxPrefix+"_TOKEN", fallback)
}

func getInfluxOrganisation(fallback string) string {
	return getStringEnv(influxPrefix+"_ORG", fallback)
}

func getInfluxBucket(fallback string) string {
	return getStringEnv(influxPrefix+"_BUCKET", fallback)
}

func getInfluxUsername(fallback string) string {
	return getStringEnv(influxPrefix+"_USERNAME", fallback)
}

func getInfluxPassword(fallback string) string {
	return getStringEnv(influxPrefix+"_PASSWORD", fallback)
}

func getInfluxBatchSize(fallback uint64) uint64 {
	return getUIntEnv("INFLUXDB_BATCH_SIZE", fallback)
}
