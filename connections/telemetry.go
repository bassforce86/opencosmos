package connections

import (
	"fmt"
	"net"
	"os"
)

const telemetryPrefix = "TELEMETRY"

type TelemetryConnection struct {
	base Connection
}

func Telemetry() *TelemetryConnection {
	return &TelemetryConnection{
		base: Connection{
			Protocol: "tcp",
			Host:     getTelemetryHost("localhost"),
			Port:     getTelemetryPort("8000"),
		},
	}
}

func (c *TelemetryConnection) Connect() net.Conn {
	// Listen for incoming connections.
	conn, err := net.Dial(c.Protocol(), fmt.Sprintf("%s:%s", c.Host(), c.Port()))
	if err != nil {
		fmt.Printf("Error dialling: %s\n", err.Error())
		os.Exit(2)
	}
	fmt.Printf("Connected to Telemetry: %+v\n", c)
	fmt.Println("Awaiting messages...")

	return conn
}

func (c *TelemetryConnection) Host() string {
	return c.base.Host
}

func (c *TelemetryConnection) Port() string {
	return c.base.Port
}

func (c *TelemetryConnection) Protocol() string {
	return c.base.Protocol
}

func getTelemetryHost(fallback string) string {
	return getStringEnv(telemetryPrefix+"_HOST", fallback)
}

func getTelemetryPort(fallback string) string {
	return getStringEnv(telemetryPrefix+"_PORT", fallback)
}
